namespace Splorr.Spacefarers

type EntityType =
    | StarPort of StarPort

type Entity =
    {
        entityType: EntityType
        location: Location
    }

module Entity =
    let canDock (avatar:Avatar) (entity:Entity) : bool =
        match entity.entityType with
        | StarPort _ ->
            (Location.distance entity.location avatar.location) < avatar.vessel.dockingDistance

    let getName (entity:Entity) : string =
        match entity.entityType with
        | StarPort s ->
            s.name

    let generateDelivery (random:System.Random) (ids:string list) (entity:Entity) : Entity =
        match entity.entityType with
        | StarPort sp ->
            {entity with entityType = sp |> StarPort.generateDelivery random ids |> StarPort}

    let isStarPort (entity:Entity) : bool =
        match entity.entityType with
        | StarPort _ ->
            true

    let getMarketCommodity (commodity:Commodity) (entity:Entity) : MarketCommodity option =
        match entity.entityType with
        | StarPort s ->
            s |> StarPort.getMarketCommodity commodity

