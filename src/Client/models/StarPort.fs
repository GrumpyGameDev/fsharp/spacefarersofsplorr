namespace Splorr.Spacefarers

type MarketCommodity =
    {
        supply: float
        demand: float
    }

module MarketCommodity =
    let create (random:System.Random) : MarketCommodity =
        {
            supply=(Utility.generateFloat random 1.0 6.0) + (Utility.generateFloat random 1.0 6.0) + (Utility.generateFloat random 1.0 6.0)
            demand=(Utility.generateFloat random 1.0 6.0) + (Utility.generateFloat random 1.0 6.0) + (Utility.generateFloat random 1.0 6.0)
        }

type StarPort =
    {
        name     : string
        delivery : Delivery
        market   : Map<Commodity, MarketCommodity>
    }

module StarPort =
    let private createMarket (random: System.Random) : Map<Commodity,MarketCommodity> =
        Commodity.list
        |> List.map
            (fun i ->
                (i, random |> MarketCommodity.create))
        |> Map.ofList

    let create (random: System.Random) (name:string) (delivery:Delivery) : StarPort =
        {
            name     = name
            delivery = delivery
            market   = random |> createMarket
        }

    let setDelivery (delivery:Delivery) (starPort:StarPort) : StarPort =
        {starPort with delivery = delivery}

    let generateDelivery (random:System.Random) (ids:string list) (starPort:StarPort) : StarPort =
        { starPort with delivery = Delivery.create (ids |> List.minBy (fun _ -> random.Next())) (random.NextDouble() * 5.0<``Deca¥``> + 5.0<``Deca¥``>)}

    let getMarketCommodity (commodity:Commodity) (starPort:StarPort) : MarketCommodity option =
        starPort.market
        |> Map.tryFind commodity