namespace Splorr.Spacefarers

type Vessel =
    {
        maximumSpeed    : float<LightYear/Turn>
        minimumSpeed    : float<LightYear/Turn>
        dockingDistance : float<LightYear>
        components      : Map<string, VesselComponent>
    }

module Vessel =
    let create () : Vessel =
        {
            maximumSpeed    =  1.0<LightYear/Turn> //TODO: this moves to Drive
            minimumSpeed    =  0.0<LightYear/Turn>
            dockingDistance =  1.0<LightYear>
            components      = Map.empty
        }

    let private setComponents (components: Map<string, VesselComponent>) (vessel:Vessel) : Vessel =
        {vessel with components = components}

    let private transformComponents (transform:Map<string,VesselComponent> -> Map<string,VesselComponent>) (vessel:Vessel) : Vessel =
        {vessel with components = vessel.components |> transform }

    let setComponent (id:string) (``component``:VesselComponent) =
        transformComponents (Map.add id ``component``)

    let addComponent (``component``:VesselComponent) =
        setComponent (VesselComponent.newId()) ``component``

    let removeComponent (id:string) =
        transformComponents (Map.remove id)

    let getAvailableEnergy (vessel:Vessel) : float<``Megasthène``> =
        vessel.components
        |> Map.map
            (fun _ v ->
                v
                |> VesselComponent.toBattery
                |> Option.map (fun b -> b.current)
                |> Option.defaultValue 0.0<``Megasthène``>)
        |> Map.toList
        |> List.map snd
        |> List.reduce (+)

    let getEnergyCostAndSpeed (vessel:Vessel) : float<``Megasthène``> * float<LightYear/Turn> =
        let availableEnergy =
            vessel
            |> getAvailableEnergy

        vessel.components
        |> Map.filter (fun _ v -> v |> VesselComponent.isDrive)
        |> Map.map (fun _ v -> v |> VesselComponent.toDrive |> Option.get)
        |> Map.fold (fun (energy, (cost, speed)) _ v ->
            let (energyCost, speedIncrease) =
                v
                |> Drive.calculateEnergyToSpeed energy
            (energy - energyCost, (cost + energyCost, speed + speedIncrease))) (availableEnergy, (0.0<``Megasthène``>, 0.0<LightYear/Turn>))
        |> snd

    let setThrottle (id:string) (throttle:float) (vessel:Vessel) : Vessel =
        match vessel.components.TryFind id with
        | Some (Reactor r) ->
            vessel
            |> transformComponents (Map.add id (r |> Reactor.setThrottle throttle |> Reactor))
        | Some (Drive d) ->
            vessel
            |> transformComponents (Map.add id (d |> Drive.setThrottle throttle |> Drive))
        | _ ->
            vessel

    let private getComponents (vessel:Vessel) : Map<string,VesselComponent> =
        vessel.components

    let private getFuelSuppliesMap : Vessel -> Map<string, FuelSupply> =
        getComponents
        >> Map.fold
            (fun a k v ->
                match v |> VesselComponent.toFuelsupply with
                | Some fs ->
                    a
                    |> Map.add k fs
                | _ ->
                    a) Map.empty

    let private getBatteriesMap : Vessel -> Map<string, Battery> =
        getComponents
        >> Map.fold
            (fun a k v ->
                match v |> VesselComponent.toBattery with
                | Some b ->
                    a
                    |> Map.add k b
                | _ ->
                    a) Map.empty

    let private getFuelAvailable =
        getFuelSuppliesMap
        >> Map.fold (fun a _ v -> a + v.current) 0.0<``Megasthène``>

    let private getReactorList =
        getComponents
        >> Map.fold
            (fun a k v ->
                match v |> VesselComponent.toReactor with
                | Some r ->
                    a
                    |> List.append [ r ]
                | _ ->
                    a) []

    let private updateFuelSupplies (fuelCost: float<``Megasthène``>) (vessel:Vessel) =
        vessel
        |> getFuelSuppliesMap
        |> Map.fold
            (fun (fs:Map<string,FuelSupply>, left:float<``Megasthène``>) k (v:FuelSupply) ->
                if left < v.current then
                    (fs
                    |> Map.add k (v |> FuelSupply.setCurrent (v.current - left))
                    ,0.0<``Megasthène``>)
                else
                    (fs
                    |> Map.add k (v |> FuelSupply.setCurrent 0.0<``Megasthène``>)
                    ,left-v.current)) (Map.empty, fuelCost)
        |> fst

    let private updateBatteries (energyAdded: float<``Megasthène``>) (vessel:Vessel) =
        vessel
        |> getBatteriesMap
        |> Map.fold
            (fun (b:Map<string,Battery>, left:float<``Megasthène``>) k v ->
                if v.maximum - v.current < left then
                    (b
                    |> Map.add k (v |> Battery.setCurrent (v.maximum))
                    ,left - v.maximum + v.current)
                else
                    (b
                    |> Map.add k (v |> Battery.setCurrent (v.current + left))
                    ,0.0<``Megasthène``>)) (Map.empty, energyAdded)
        |> fst

    let applyTurns (turns:float<Turn>) (vessel:Vessel) : Vessel =
        let fuelSupplies =
            vessel |> getFuelSuppliesMap
        let fuelAvailable =
            vessel |> getFuelAvailable

        let reactors =
            vessel |> getReactorList
        let fuelConsumptionRate =
            reactors
            |> List.fold (fun a v -> a + (v |> Reactor.getFuelConsumptionRate)) 0.0<``Megasthène``/Turn>
        let powerGenerationTurns =
            (fuelAvailable/fuelConsumptionRate) |> min turns
        let reactorProduction =
            reactors
            |> List.map
                (Reactor.generate powerGenerationTurns)
            |> List.reduce ReactorProduction.add

        let updatedFuelSupplies =
            vessel
            |> updateFuelSupplies reactorProduction.input

        let updatedBatteries =
            vessel
            |> updateBatteries reactorProduction.output

        (vessel.components
        |> Map.foldBack
            (fun k v a ->a |> Map.add k (v |> FuelSupply)) updatedFuelSupplies
        |> Map.foldBack
            (fun k v a ->a |> Map.add k (v |> Battery)) updatedBatteries
        |> setComponents) vessel

    let applyEnergyCost (cost:float<``Megasthène``>) (vessel:Vessel) : Vessel =
        vessel
        |> transformComponents
            (fun components ->
                ((cost, components), components)
                ||> Map.fold
                    (fun (remainingCost, updatedComponents) k v ->
                        let (costApplied, updatedComponent) =
                            v |> VesselComponent.applyEnergyCost remainingCost
                        (remainingCost - costApplied, updatedComponents |> Map.add k updatedComponent))
                |> snd)

    let getPassiveSensorRange (vessel:Vessel) : float<LightYear> =
        vessel.components
        |> Map.map
            (fun _ v ->
                v
                |> VesselComponent.toPassiveSensor
                |> Option.map (fun x -> x.range)
                |> Option.defaultValue 0.0<LightYear>)
        |> Map.toList
        |> List.map snd
        |> List.reduce max

    let private getProbingSensorsList =
        getComponents
        >> Map.fold
            (fun a k v ->
                match v |> VesselComponent.toProbingSensor with
                | Some ps ->
                    a
                    |> List.append [ ps ]
                | _ ->
                    a) []

    let private getMaximumProbeRange (vessel:Vessel) : float<LightYear> =
        let availableEnergy = vessel |> getAvailableEnergy
        vessel
        |> getProbingSensorsList
        |> List.map
            (fun ps ->
                (Conversions.solve availableEnergy ps.conversion
                |> List.reduce max) * 1.0<LightYear>
                |> min ps.range)
        |> List.reduce max

    let private canProbeDistance (distance:float<LightYear>) (vessel:Vessel) : bool =
        (vessel |> getMaximumProbeRange) >= (abs distance)

    let private getProbeEnergyCost (distance:float<LightYear>) (vessel:Vessel) : (float<``Megasthène``>) option =
        if vessel |> canProbeDistance distance then
            vessel
            |> getProbingSensorsList
            |> List.filter
                (fun ps -> ps.range >= distance)
            |> List.map
                (fun ps -> Conversions.calculate distance ps.conversion)
            |> List.reduce min
            |> Some
        else
            None

    let canProbe (distance:float<LightYear>) (vessel:Vessel) : bool =
        vessel
        |> getProbeEnergyCost distance
        |> Option.fold
            (fun _ i ->
                (vessel |> getAvailableEnergy) >= i) false

    let probe (distance:float<LightYear>) (vessel:Vessel) : Vessel * (string list) =
        vessel
        |> getProbeEnergyCost distance
        |> Option.fold
            (fun (a,l) i ->
                (a |> applyEnergyCost i, [ (i,distance) ||> sprintf "DEBUG: Applied energy cost %f for distance %f" ] |> List.append l)) (vessel, [])

    let getCommodityCargo (commodity:Commodity) (vessel:Vessel) : float<Serplathe> =
        vessel.components
        |> Map.toList
        |> List.map (snd >> VesselComponent.getCommodityCargo commodity)
        |> List.reduce (+)

    let getCargoSpaceAvailable (vessel:Vessel) : float<Serplathe> =
        vessel.components
        |> Map.toList
        |> List.map (snd >> VesselComponent.getCargoSpaceAvailable)
        |> List.reduce (+)

    let addCommodityCargo (commodity:Commodity) (amount:float<Serplathe>) (vessel:Vessel) : Vessel =
        let updatedComponents =
            vessel.components
            |> Map.fold
                (fun (a, c) k v->
                    let updatedAmount, updatedComponent =
                        v
                        |> VesselComponent.addCommodityCargo commodity a
                    (updatedAmount, c
                    |> Map.add k updatedComponent)) (amount, Map.empty)
            |> snd

        vessel
        |> setComponents updatedComponents
