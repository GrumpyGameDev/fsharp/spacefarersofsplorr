namespace Splorr.Spacefarers

type VesselComponent =
    | FuelSupply of FuelSupply
    | Battery of Battery
    | Reactor of Reactor
    | Drive of Drive
    | PassiveSensor of PassiveSensor
    | ProbingSensor of ProbingSensor
    | CargoHold of CargoHold

module VesselComponent =
    let newId () = System.Guid.NewGuid().ToString()

    let componentTypeName (``component``:VesselComponent) : string =
        match ``component`` with
        | FuelSupply _ ->
            "Fuel Supply"
        | Battery _ ->
            "Battery"
        | Reactor _ ->
            "Reactor"
        | Drive _ ->
            "Drive"
        | PassiveSensor _ ->
            "Passive Sensor"
        | ProbingSensor _ ->
            "Probing Sensor"
        | CargoHold _ ->
            "Cargo Hold"

    let createFuelSupply (maximum:float<``Megasthène``>) : VesselComponent =
        maximum
        |> FuelSupply.create
        |> FuelSupply

    let createBattery (maximum:float<``Megasthène``>) : VesselComponent =
        maximum
        |> Battery.create
        |> Battery

    let createReactor (conversion:Conversion<``Megasthène``, ``Megasthène``>) (maximumInput:float<``Megasthène``/Turn>) : VesselComponent =
        (conversion, maximumInput)
        ||> Reactor.create
        |> Reactor

    let createDrive (conversion:Conversion<``Megasthène``, LightYear>) (maximumSpeed:float<LightYear/Turn>) : VesselComponent =
        (conversion, maximumSpeed)
        ||> Drive.create
        |> Drive

    let createPassiveSensor (range:float<LightYear>) : VesselComponent =
        range
        |> PassiveSensor.create
        |> PassiveSensor

    let createProbingSensor (conversion:Conversion<``Megasthène``, LightYear>) (range:float<LightYear>) : VesselComponent =
        (conversion, range)
        ||> ProbingSensor.create
        |> ProbingSensor

    let createCargoHold (capacity:float<Serplathe>) :VesselComponent =
        capacity
        |> CargoHold.create
        |> CargoHold

    let isReactor = function
        | Reactor _ -> true
        | _ -> false

    let toReactor = function
        | Reactor x -> x |> Some
        | _ -> None

    let isFuelSupply = function
        | FuelSupply _ -> true
        | _ -> false

    let toFuelsupply = function
        | FuelSupply x -> x |> Some
        | _ -> None

    let isBattery = function
        | Battery _ -> true
        | _ -> false

    let toBattery = function
        | Battery x -> x |> Some
        | _ -> None

    let isDrive = function
        | Drive _ -> true
        | _ -> false

    let toDrive = function
        | Drive x -> x |> Some
        | _ -> None

    let toPassiveSensor = function
        | PassiveSensor x -> x |> Some
        | _ -> None

    let toProbingSensor = function
        | ProbingSensor x -> x |> Some
        | _ -> None

    let toCargoHold = function
        | CargoHold x -> x |> Some
        | _ -> None

    let applyEnergyCost (cost:float<``Megasthène``>) (``component``:VesselComponent) : float<``Megasthène``> * VesselComponent =
        match ``component`` with
        | Battery b ->
            let (appliedCost, updatedBattery) =
                b
                |> Battery.applyEnergyCost cost
            (appliedCost, updatedBattery |> Battery)
        | _ ->
            (0.0<``Megasthène``>, ``component``)

    let getCommodityCargo (commodity:Commodity) (``component``:VesselComponent) : float<Serplathe> =
        match ``component`` with
        | CargoHold c ->
            c
            |> CargoHold.getCommodityCargo commodity
        | _ ->
            0.0<Serplathe>

    let getCargoSpaceAvailable (``component``:VesselComponent) : float<Serplathe> =
        match ``component`` with
        | CargoHold c ->
            c
            |> CargoHold.getCargoSpaceAvailable
        | _ ->
            0.0<Serplathe>

    let addCommodityCargo (commodity:Commodity) (amount:float<Serplathe>) (``component``:VesselComponent) : float<Serplathe> * VesselComponent =
        match ``component`` with
        | CargoHold c ->
            let updatedAmount, updatedCargoHold =
                c
                |> CargoHold.addCargo commodity amount
            (updatedAmount, updatedCargoHold |> CargoHold)
        | x ->
            (amount, x)


