namespace Splorr.Spacefarers

type ProbingSensor =
    {
        range: float<LightYear>
        conversion: Conversion<``Megasthène``, LightYear>
    }

module ProbingSensor =
    let create (conversion:Conversion<``Megasthène``, LightYear>) (range:float<LightYear>) : ProbingSensor =
        {
            range      = range
            conversion = conversion
        }

    let calculateEnergyForRange (range: float<LightYear>) (sensor:ProbingSensor) : float<``Megasthène``> =
        if range > sensor.range then
            1.0<``Megasthène``> * infinity
        else
            0.0<``Megasthène``>


