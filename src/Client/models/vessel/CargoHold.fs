namespace Splorr.Spacefarers


type CargoHold =
    {
        capacity: float<Serplathe>
        cargo: Map<Commodity,float<Serplathe>>
    }

module CargoHold =
    let create (capacity:float<Serplathe>) : CargoHold =
        {
            capacity = capacity
            cargo = Map.empty
        }

    let getCommodityCargo (commodity:Commodity) (cargoHold:CargoHold) : float<Serplathe> =
        cargoHold.cargo
        |> Map.tryFind commodity
        |> Option.defaultValue 0.0<Serplathe>

    let getCargoSpaceAvailable (cargoHold:CargoHold) : float<Serplathe> =
        cargoHold.cargo
        |> Map.fold
            (fun a _ v ->
                a - v) cargoHold.capacity

    let private getCommodity (commodity:Commodity) (cargoHold:CargoHold) : float<Serplathe> =
        cargoHold.cargo
        |> Map.tryFind commodity
        |> Option.defaultValue 0.0<Serplathe>

    let private transformCargo (transform:Map<Commodity,float<Serplathe>> -> Map<Commodity,float<Serplathe>>) (cargoHold:CargoHold) : CargoHold =
        {cargoHold with cargo = cargoHold.cargo |> transform}

    let private setCargo (commodity:Commodity) (amount:float<Serplathe>) =
        transformCargo (Map.add commodity amount)

    let private changeCargoBy (commodity:Commodity) (amount:float<Serplathe>) (cargoHold:CargoHold) : CargoHold =
        cargoHold
        |> setCargo commodity (amount + (cargoHold |> getCommodity commodity))

    let addCargo (commodity:Commodity) (amount:float<Serplathe>) (cargoHold:CargoHold) : float<Serplathe> * CargoHold =
        let stored = (cargoHold |> getCargoSpaceAvailable) |> min amount
        let updatedCargoHold =
            cargoHold
            |> changeCargoBy commodity stored
        let updatedAmount = amount - stored
        (updatedAmount, updatedCargoHold)


