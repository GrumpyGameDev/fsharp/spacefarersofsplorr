namespace Splorr.Spacefarers


type Reactor =
    {
        maximumInput: float<``Megasthène``/Turn>
        throttle: float //0.0 - 1.0
        conversion: Conversion<``Megasthène``, ``Megasthène``>
    }

//TODO: this one moves into its own file?
type ReactorProduction =
    {
        input: float<``Megasthène``>
        output: float<``Megasthène``>
    }

module ReactorProduction =
    let create () : ReactorProduction =
        {
            input = 0.0<``Megasthène``>
            output = 0.0<``Megasthène``>
        }

    let add (first:ReactorProduction) (second:ReactorProduction) : ReactorProduction =
        {
            input = first.input + second.input
            output = first.output + second.output
        }

module Reactor =
    let create (conversion:Conversion<``Megasthène``, ``Megasthène``>) (maximumInput:float<``Megasthène``/Turn>) : Reactor =
        {
            maximumInput = maximumInput
            throttle = 1.0
            conversion = conversion
        }

    let setThrottle (throttle:float) (reactor:Reactor) : Reactor =
        {reactor with throttle = throttle |> max 0.0 |> min 1.0}

    let generate (turns:float<Turn>) (reactor:Reactor) : ReactorProduction =
        let input = turns * reactor.maximumInput * reactor.throttle
        {
            input = input
            output = reactor.conversion |> Conversions.calculate input
        }

    let getFuelConsumptionRate (v:Reactor) =
        v.throttle * v.maximumInput



