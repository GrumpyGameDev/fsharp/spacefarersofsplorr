namespace Splorr.Spacefarers

type Drive =
    {
        maximumSpeed: float<LightYear/Turn>
        throttle: float //0.0 - 1.0
        conversion: Conversion<``Megasthène``, LightYear>
    }

module Drive =
    let create (conversion:Conversion<``Megasthène``, LightYear>) (maximumSpeed:float<LightYear/Turn>) : Drive =
        {
            maximumSpeed = maximumSpeed
            throttle = 1.0
            conversion = conversion
        }

    let setThrottle (throttle:float) (drive:Drive) : Drive =
        {drive with throttle = throttle |> max 0.0 |> min 1.0}

    let calculateEnergyToSpeed (energyAvailable:float<``Megasthène``>) (drive:Drive) : float<``Megasthène``> * float<LightYear/Turn> =
        let desiredSpeed = drive.throttle * drive.maximumSpeed
        let actualEnergy = drive.conversion |> Conversions.calculate (desiredSpeed * 1.0<Turn>)
        if(actualEnergy <= energyAvailable) then
            (actualEnergy, desiredSpeed)
        else
            let solution =
                drive.conversion
                |> Conversions.solve energyAvailable
                |> List.head
            (energyAvailable, solution * 1.0<LightYear/Turn>)


