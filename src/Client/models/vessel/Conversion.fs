namespace Splorr.Spacefarers

type Conversion<[<Measure>]'T,[<Measure>]'U> =
    {
        squared:  float<'T/'U^2>
        linear:   float<'T/'U>
        constant: float<'T>
    }

module Conversions =
    let create (squared:float<'T/'U^2>, linear:float<'T/'U>,constant: float<'T>) : Conversion<'T,'U> =
        {
            squared = squared
            linear = linear
            constant = constant
        }

    let calculate (input:float<'U>) (conversion:Conversion<'T,'U>) : float<'T> =
        conversion.constant + conversion.linear * input + conversion.squared * input * input

    let solve (output:float<'T>) (conversion:Conversion<'T,'U>) : (float list) =
        let a : float = conversion.squared |> float
        let b : float = conversion.linear |> float
        let c : float = (conversion.constant - output) |> float
        match a, (b*b - 4.0 * a * c) with
        | 0.0,  _ ->
            [ (-c/b) ]
        | _, x when x < 0.0 ->
            []
        | _, x when x > 0.0 ->
            [ ((-b + sqrt(b*b - 4.0 * a * c)) / (2.0 * a)); ((-b - sqrt(b*b - 4.0 * a * c)) / (2.0 * a)) ]
        | _ ->
            [ (-b / (2.0 * a))]


