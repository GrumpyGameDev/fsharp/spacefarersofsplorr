namespace Splorr.Spacefarers

type Battery =
    {
        maximum: float<``Megasthène``>
        current: float<``Megasthène``>
    }

module Battery =
    let create (maximum:float<``Megasthène``>) : Battery =
        {
            maximum = maximum
            current = maximum
        }

    let setCurrent (current:float<``Megasthène``>) (battery:Battery) : Battery =
        {battery with current = current |> max 0.0<``Megasthène``> |> min battery.maximum}

    let applyEnergyCost (cost:float<``Megasthène``>) (battery:Battery) : float<``Megasthène``> * Battery =
        let appliedCost = cost |> min battery.current
        (appliedCost, battery |> setCurrent (battery.current - appliedCost))


