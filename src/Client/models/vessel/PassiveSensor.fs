namespace Splorr.Spacefarers

type PassiveSensor =
    {
        range: float<LightYear>
    }

module PassiveSensor =
    let create (range:float<LightYear>) : PassiveSensor =
        {
            range = range
        }


