namespace Splorr.Spacefarers

type FuelSupply =
    {
        maximum: float<``Megasthène``>
        current: float<``Megasthène``>
    }

module FuelSupply =
    let create (maximum:float<``Megasthène``>) : FuelSupply =
        {
            maximum = maximum
            current = maximum
        }

    let setCurrent (current:float<``Megasthène``>) (fuelSupply:FuelSupply) : FuelSupply =
        {fuelSupply with current = current |> max 0.0<``Megasthène``> |> min fuelSupply.maximum}
