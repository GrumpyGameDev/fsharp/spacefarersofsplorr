namespace Splorr.Spacefarers

type Game =
    {
        state: GameState
        random: System.Random
        avatar: Avatar
        entities: Map<string, Entity>
        turnCount: float<Turn>
        commodities: Map<Commodity, CommodityDescriptor>
    }

module Game =
    let formatTurns (value:float<Turn>) : string =
        value
        |> sprintf "%8.2F"

    let rec private generateStarLocations (random:System.Random) (tryCount:int) (currentTry:int) (xSize:float<LightYear>,ySize:float<LightYear>,zSize:float<LightYear>) (minimumDistance:float<LightYear>) (locations:Location list) : Location list =
        if currentTry>=tryCount then
            locations
        else
            let location = (random.NextDouble() * xSize, random.NextDouble() * ySize, random.NextDouble() * zSize)
            if locations |> List.forall (fun l -> (l |> Location.distance location) >= minimumDistance) then
                generateStarLocations random tryCount 0 (xSize,ySize,zSize) minimumDistance (locations |> List.append [location])
            else
                generateStarLocations random tryCount (currentTry+1) (xSize,ySize,zSize) minimumDistance (locations)

    let rec private generateName (random:System.Random) : string =
        let consonants = [| "h"; "k"; "l"; "m"; "p" |]
        let vowels = [| "a"; "e"; "i"; "o"; "u" |]
        let nameLength = random.Next(4) + random.Next(4) + random.Next(4) + 3
        let vowel = random.Next(2) = 1
        [1..nameLength]
        |> List.fold
            (fun (s,v) _ ->
                if v then
                    (s + vowels.[random.Next(vowels.Length)],not v)
                else
                    (s + consonants.[random.Next(consonants.Length)],not v)) ("",vowel)
        |> fst

    let rec private generateNames (random:System.Random) (count:int) (names:Set<string>) : string list =
        if names.Count >= count then
            names
            |> Set.toList
        else
            names
            |> Set.add (generateName random)
            |> generateNames random count

    let addStarPort (random:System.Random) (name:string) (ids:string list) : EntityType =
        StarPort.create random name { destination=""; reward=0.0<``Deca¥``> }
        |> StarPort.generateDelivery random ids
        |> StarPort

    let private addStarPorts  (size:float<LightYear>*float<LightYear>*float<LightYear>) (minimumDistance:float<LightYear>) (game:Game) : Game =
        let locations: Location list = generateStarLocations game.random 5000 0 size minimumDistance []
        let ids =
            [1..locations.Length]
            |> List.map (fun _ -> System.Guid.NewGuid().ToString())
        let entities =
            generateNames game.random (locations.Length) Set.empty
            |> List.zip locations
            |> List.zip ids
            |> List.fold
                (fun a (id, (location, name)) ->
                    a
                    |> Map.add
                        id
                        {
                            Entity.location=location;
                            entityType=addStarPort game.random name (ids |> List.filter (fun item -> item<>id))
                        }) Map.empty
        {game with entities = entities}

    let create (size:float<LightYear>*float<LightYear>*float<LightYear>) (density:float<LightYear>) : Game =
        let vessel =
            Vessel.create()
            |> Vessel.addComponent (VesselComponent.createFuelSupply 1000000.0<``Megasthène``>)
            |> Vessel.addComponent (VesselComponent.createBattery 10.0<``Megasthène``>)
            |> Vessel.addComponent (VesselComponent.createReactor (Conversions.create(0.0<1/``Megasthène``>, 0.99, 0.0<``Megasthène``>)) 2.0<``Megasthène``/Turn>)
            |> Vessel.addComponent (VesselComponent.createDrive (Conversions.create(0.0<``Megasthène``/LightYear^2>, 1.98<``Megasthène``/LightYear>,0.0<``Megasthène``>)) 1.0<LightYear/Turn>  )
            |> Vessel.addComponent (VesselComponent.createPassiveSensor 10.0<LightYear>)
            |> Vessel.addComponent (VesselComponent.createProbingSensor (Conversions.create(0.25<``Megasthène``/LightYear^2>, 0.0<``Megasthène``/LightYear>, 0.0<``Megasthène``>)) 5.0<LightYear>)
            |> Vessel.addComponent (VesselComponent.createCargoHold 10.0<Serplathe>)
        let random = System.Random()
        {
            random = random
            avatar = Avatar.create random vessel size
            state = InSpace
            entities = Map.empty
            turnCount = 0.0<Turn>
            commodities = CommodityDescriptor.defaults
        }
        |> addStarPorts size density

    let setState (state:GameState) (game:Game) : Game  =
        {game with state = state}

    let private transformAvatar (transform:Avatar->Avatar) (game:Game) =
        {game with avatar = game.avatar |> transform}

    let setHeading (heading:float<Degree> * float<Degree>) (game:Game) =
        game
        |> transformAvatar (Avatar.setHeading (Avatar.addMessage game.turnCount) heading)
        |> setState InSpace

    let addTurns (turns:float<Turn>) (game:Game) : Game =
        { game with turnCount = game.turnCount + turns }
        |> transformAvatar (Avatar.applyTurns turns)

    let wait =
        addTurns 1.0<Turn>

    let moveAvatar (game:Game) : Game =
        game
        |> transformAvatar (Avatar.move (Avatar.addMessage game.turnCount))
        |> addTurns 1.0<Turn>

    let getNearbyEntities (game:Game) : (string * (Entity * bool)) list =
        game.entities
        |> Map.filter
            (fun _ v ->
                (v.location |> Location.distance game.avatar.location) <= (game.avatar.vessel |> Vessel.getPassiveSensorRange))
        |> Map.map
            (fun _ v -> (v, game.avatar.vessel |> Vessel.canProbe (v.location |> Location.distance game.avatar.location)))
        |> Map.toList

    let getKnownEntities (game:Game) : (string * (Entity * bool)) list =
        game.entities
        |> Map.filter
            (fun k _ ->
                game.avatar.visitCounts.ContainsKey k)
        |> Map.map
            (fun _ v -> (v, game.avatar.vessel |> Vessel.canProbe (v.location |> Location.distance game.avatar.location)))
        |> Map.toList

    let headFor (id:string) (game:Game) : Game =
        match game.entities |> Map.tryFind id with
        | Some e ->
            game
            |> transformAvatar
                (Avatar.setHeading
                    (Avatar.addMessage game.turnCount)
                    (Location.thetaTo game.avatar.location e.location, Location.phiTo game.avatar.location e.location))
        | _ ->
            game

    let private getEntityName (id:string) (game:Game) : string =
        game.entities
        |> Map.tryFind id
        |> Option.map Entity.getName
        |> Option.defaultValue ""

    let canDock (id:string) (game:Game) : bool =
        game.entities
        |> Map.tryFind id
        |> Option.map (Entity.canDock game.avatar)
        |> Option.defaultValue false

    let dock (id:string) (game:Game) : Game =
        if game |> canDock id then
            {game with state = (id, AtDock) |> Docked}
            |> transformAvatar (Avatar.tryDeliver (Avatar.addMessage game.turnCount) id (game |> getEntityName id))
            |> transformAvatar (Avatar.dock  (Avatar.addMessage game.turnCount) id (game |> getEntityName id))
        else
            game

    let undock (game:Game) : Game =
        match game.state with
        | Docked _ ->
            game
            |> setState InSpace
        | _ ->
            game

    let viewVesselStatus (game:Game) : Game =
        match game.state with
        | VesselStatus _ ->
            game
        | Status x ->
            game
            |> setState (x |> VesselStatus)
        | x ->
            game
            |> setState (x |> VesselStatus)

    let viewEntityStatus (id:string) (game:Game) : Game =
        match game.state with
        | EntityStatus _ ->
            game
        | x ->
            game
            |> setState ((id,x) |> EntityStatus)

    let viewStatus (game:Game) : Game =
        match game.state with
        | Status _ ->
            game
        | VesselStatus x ->
            game
            |> setState (x |> Status)
        | x ->
            game
            |> setState (x |> Status)

    let goBack (game:Game) : Game =
        match game.state with
        | Status s ->
            game |> setState s
        | VesselStatus s ->
            game |> setState s
        | EntityStatus (_,s) ->
            game |> setState s
        | _ ->
            game

    let abandonDelivery (game:Game) =
        game
        |> transformAvatar (Avatar.abandonDelivery (Avatar.addMessage game.turnCount))

    let getAvailableDelivery (game:Game) : Delivery option =
        match game.state with
        | Docked (id, _) ->
            match game.entities.[id].entityType with
            | StarPort port ->
                port.delivery |> Some
        | _ ->
            None

    let pendDelivery (game:Game) : Game =
        match game.state with
        | Docked (id, AtDock) ->
            game
            |> getAvailableDelivery
            |> Option.fold
                (fun g _ ->
                    g
                    |> setState (Docked (id, PendingDelivery))) game
        | _ ->
            game

    let private setEntity (id:string) (entity:Entity) (game:Game) : Game =
        {game with entities = game.entities |> Map.add id entity}

    let transformEntity (id:string) (transform:Entity->Entity) (game:Game) : Game =
        game.entities
        |> Map.tryFind id
        |> Option.fold
            (fun g e ->
                g
                |> setEntity id (e |> transform)) game

    let getStarPortIds (excluded:string option) (game:Game) : string list =
        game.entities
        |> Map.filter
            (fun k v -> v |> Entity.isStarPort)
        |> Option.foldBack
            (fun x e ->
                e
                |> Map.remove x) excluded
        |> Map.toList
        |> List.map fst

    let acceptDelivery (game:Game) : Game =
        match game.state with
        | Docked (id, PendingDelivery) ->
            game
            |> getAvailableDelivery
            |> Option.fold
                (fun g d ->
                    g
                    |> transformEntity id (Entity.generateDelivery game.random (g |> getStarPortIds (id |> Some)))
                    |> transformAvatar (Avatar.acceptDelivery (Avatar.addMessage game.turnCount) d (g |> getEntityName d.destination))
                    |> setState (Docked (id, AtDock))) game
        | _ ->
            game

    let unpendDelivery (game:Game) : Game =
        match game.state with
        | Docked (id, PendingDelivery) ->
            game
            |> setState (Docked (id, AtDock))
        | _ ->
            game

    let getThrottle (id:string) (game:Game) : float option=
        match game.avatar.vessel.components |> Map.tryFind id with
        | Some (Reactor r) ->
            r.throttle |> Some
        | Some (Drive d) ->
            d.throttle |> Some
        | _ ->
            None

    let beginSetThrottle (id:string) (game:Game) : Game =
        match (game |> getThrottle id, game.state) with
        | (Some v, SetThrottle (_,_,s)) ->
            game |> setState (SetThrottle (id, v, s))
        | (Some v, VesselStatus x) ->
            game |> setState (SetThrottle (id, v, x))
        | _ -> game

    let changeThrottle (id:string) (throttle:float) (game:Game) : Game =
        match (game |> getThrottle id, game.state) with
        | (Some _, SetThrottle (i,_,s)) when id = i ->
            game |> setState (SetThrottle (id, throttle, s))
        | _ -> game

    let issueSetThrottle (id:string) (throttle:float) (game:Game) : Game =
        match (game |> getThrottle id, game.state) with
        | (Some _, SetThrottle (i,_,s)) when id = i ->
            game
            |> transformAvatar (Avatar.setThrottle id throttle)
            |> setState (VesselStatus s)
        | _ -> game

    let belaySetThrottle (game:Game) : Game =
        match game.state with
        | SetThrottle (_,_,s) ->
            game
            |> setState (VesselStatus s)
        | _ -> game

    let getCommodityCargo (commodity:Commodity) (game:Game) : float<Serplathe> =
        game.avatar.vessel
        |> Vessel.getCommodityCargo commodity

    let beginCommodityBuy (commodity:Commodity) (game:Game) : Game =
        match game.state with
        | Docked (id,AtDock) ->
            game
            |> setState (Docked (id, CommodityBuy (commodity,0.0<Serplathe>)))
        | _ -> game

    let changeCommodityBuy (commodity:Commodity) (amount:float<Serplathe>) (game:Game) : Game =
        match game.state with
        | Docked (id, CommodityBuy (c, _)) when c = commodity ->
            game
            |> setState (Docked (id, CommodityBuy (commodity,amount)))
        | _ -> game

    let getMarketCommodity (id:string) (commodity:Commodity) (game:Game) : MarketCommodity option =
        game.entities
        |> Map.tryFind id
        |> Option.fold
            (fun _ entity ->
                entity |> Entity.getMarketCommodity commodity) None

    let issueCommodityBuy (descriptor:CommodityDescriptor) (marketCommodity:MarketCommodity) (commodity:Commodity) (amount:float<Serplathe>) (game:Game) : Game =
        match game.state with
        | Docked (id, CommodityBuy (c, _)) when c = commodity ->
            let actualBuy =
                (game.avatar |> Avatar.getCargoSpaceAvailable)
                |> min (game.avatar |> Avatar.getMaximumCommodityBuy descriptor marketCommodity)
                |> min amount

            game
            |> transformAvatar (Avatar.buyCommodity (Avatar.addMessage game.turnCount) game.commodities.[commodity] (game |> getMarketCommodity id commodity |> Option.get) commodity actualBuy)//TODO: affect commodity demand,also, clamp to maximum buy based on currency
            |> setState (Docked (id, AtDock))//TODO: increase demand for commodity at starport
        | _ -> game

    let belayCommodityBuy (game:Game) : Game =
        match game.state with
        | Docked (id, CommodityBuy _) ->
            game
            |> setState (Docked (id, AtDock))
        | _ -> game

    let beginCommoditySell (commodity:Commodity) (game:Game) : Game =
        match game.state with
        | Docked (id,AtDock) ->
            game
            |> setState (Docked (id, CommoditySell (commodity,0.0<Serplathe>)))
        | _ -> game

    let changeCommoditySell (commodity:Commodity) (amount:float<Serplathe>) (game:Game) : Game =
        match game.state with
        | Docked (id, CommoditySell (c, _)) when c = commodity ->
            game
            |> setState (Docked (id, CommoditySell (commodity,(min amount (game |> getCommodityCargo commodity)))))
        | _ -> game

    let issueCommoditySell (commodity:Commodity) (amount:float<Serplathe>) (game:Game) : Game =
        match game.state with
        | Docked (id, CommoditySell (c, _)) when c = commodity ->
            game
            |> transformAvatar (Avatar.sellCommodity game.commodities.[commodity] (game |> getMarketCommodity id commodity |> Option.get) commodity (min amount (game |> getCommodityCargo commodity)))//TODO: affect supply of starport
            |> setState (Docked (id, AtDock))//TODO: increase supply for commodity at starport
        | _ -> game

    let belayCommoditySell (game:Game) : Game =
        match game.state with
        | Docked (id, CommoditySell _) ->
            game
            |> setState (Docked (id, AtDock))
        | _ -> game

    let probe (id:string) (game:Game) : Game =
        game.entities
        |> Map.tryFind id
        |> Option.fold
            (fun g e ->
                let distance = g.avatar.location |> Location.distance e.location
                if g.avatar.vessel |> Vessel.canProbe distance then
                    g
                    |> transformAvatar (Avatar.addVisits 0 id)
                    |> transformAvatar (Avatar.probe (Avatar.addMessage game.turnCount) e.location)
                else
                    g) game

