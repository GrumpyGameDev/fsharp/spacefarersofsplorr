namespace Splorr.Spacefarers

type Model =
    | MainMenu
    | NewGame of ((float<LightYear> * float<LightYear> * float<LightYear>) option) * (float<LightYear> option)
    | Instructions
    | About
    | InGame of Game
