namespace Splorr.Spacefarers


type Location = float<LightYear> * float<LightYear> * float<LightYear>

module Location =
    let distance
            (x1:float<LightYear>,y1:float<LightYear>,z1:float<LightYear>)
            (x2:float<LightYear>,y2:float<LightYear>,z2:float<LightYear>)
            : float<LightYear> =
        sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2))

    let thetaTo
            (x1:float<LightYear>,y1:float<LightYear>,_:float<LightYear>)
            (x2:float<LightYear>,y2:float<LightYear>,_:float<LightYear>)
            : float<Degree> =
        atan2 (y2-y1) (x2-x1)
        |> Degrees.fromRadians

    let phiTo
            (x1:float<LightYear>,y1:float<LightYear>,z1:float<LightYear>)
            (x2:float<LightYear>,y2:float<LightYear>,z2:float<LightYear>)
            : float<Degree> =
        atan2 (z2-z1) (sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)))
        |> Degrees.fromRadians

    let format (value:float<LightYear>) : string =
        if value < 0.0<LightYear> then
            value |> abs |> sprintf "-%7.3F"
        else
            value |> abs |> sprintf "+%7.3F"

