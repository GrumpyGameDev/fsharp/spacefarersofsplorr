namespace Splorr.Spacefarers

[<Measure>]
type ``Megasthène`` //unit of energy

[<Measure>]
type Degree //unit of angular measure

[<Measure>]
type LightYear //unit of distance

[<Measure>]
type Serplathe //unit of mass

[<Measure>]
type Turn //unit of time

[<Measure>]
type ``Deca¥``


