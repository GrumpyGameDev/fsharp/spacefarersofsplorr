namespace Splorr.Spacefarers

type CommodityDescriptor =
    {
        name:           string
        basePrice:      float<``Deca¥``/Serplathe>
        markUp:         float
        salesFactor:    float
        purchaseFactor: float
    }

module CommodityDescriptor =
    let defaults =
        [
            (Water,    { name = "Water"    ; basePrice =  10.0<``Deca¥``/Serplathe>; markUp = 0.1 ; salesFactor = 0.01; purchaseFactor = 0.01 })
            (Food,     { name = "Food"     ; basePrice =  25.0<``Deca¥``/Serplathe>; markUp = 0.1 ; salesFactor = 0.01; purchaseFactor = 0.01 })
            (Ore,      { name = "Ore"      ; basePrice =  50.0<``Deca¥``/Serplathe>; markUp = 0.1 ; salesFactor = 0.01; purchaseFactor = 0.01 })
            (Metal,    { name = "Metal"    ; basePrice = 100.0<``Deca¥``/Serplathe>; markUp = 0.1 ; salesFactor = 0.01; purchaseFactor = 0.01 })
            (Textiles, { name = "Textiles" ; basePrice = 250.0<``Deca¥``/Serplathe>; markUp = 0.1 ; salesFactor = 0.01; purchaseFactor = 0.01 })
        ]
        |> Map.ofList

    let calculateSalePricePerUnit (supply:float) (demand:float) (descriptor:CommodityDescriptor) :float<``Deca¥``/Serplathe> =
        descriptor.basePrice * (demand / supply)

    let calculatePurchasePricePerUnit (supply:float) (demand:float) (descriptor:CommodityDescriptor) :float<``Deca¥``/Serplathe> =
        (calculateSalePricePerUnit supply demand descriptor) * (1.0 + descriptor.markUp)

    let calculateUnitSalePrice (supply:float) (demand:float) (descriptor:CommodityDescriptor) :float<``Deca¥``> =
        (calculateSalePricePerUnit supply demand descriptor) * 1.0<Serplathe>

    let calculateUnitPurchasePrice (supply:float) (demand:float) (descriptor:CommodityDescriptor) :float<``Deca¥``> =
        (calculatePurchasePricePerUnit supply demand descriptor) * 1.0<Serplathe>

