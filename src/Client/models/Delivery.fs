namespace Splorr.Spacefarers

type Delivery =
    {
        destination: string
        reward: float<``Deca¥``>
    }

module Delivery =
    let create (desination:string) (reward:float<``Deca¥``>) : Delivery =
        {
            destination = desination
            reward = reward
        }