namespace Splorr.Spacefarers

type Heading = float<Degree> * float<Degree>
