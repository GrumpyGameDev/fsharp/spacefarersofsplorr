namespace Splorr.Spacefarers

type Commodity =
    | Water
    | Food
    | Ore
    | Metal
    | Textiles

module Commodity =
    let list =
        [
            Water
            Food
            Ore
            Metal
            Textiles
        ]
