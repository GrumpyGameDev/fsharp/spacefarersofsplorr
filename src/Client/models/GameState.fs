namespace Splorr.Spacefarers

type DockedState =
    | AtDock
    | PendingDelivery
    | CommodityBuy of Commodity * float<Serplathe>
    | CommoditySell of Commodity * float<Serplathe>

type GameState =
    | InSpace
    | SetHeading of float<Degree> * float<Degree>
    | SetDestination of string option
    | SetThrottle of string * float * GameState
    | Docked of string * DockedState
    | Status of GameState
    | VesselStatus of GameState
    | EntityStatus of string * GameState

