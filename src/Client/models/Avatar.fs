namespace Splorr.Spacefarers

type Avatar =
    {
        location    : Location
        heading     : Heading
        statistics  : Map<AvatarStatistic,float>
        currency    : float<``Deca¥``>
        visitCounts : Map<string, int>
        delivery    : Delivery option
        messages    : (float<Turn> * string) list
        vessel      : Vessel
    }

module Avatar =
    let create (random:System.Random) (vessel:Vessel) (x:float<LightYear>,y:float<LightYear>,z:float<LightYear>) : Avatar =
        {
            location    = (x / 2.0, y / 2.0, z / 2.0)
            heading     = (random.NextDouble() * 360.0<Degree> - 180.0<Degree>, random.NextDouble() * 180.0<Degree> - 90.0<Degree>)
            statistics  = Map.empty
            visitCounts = Map.empty
            delivery    = None
            messages    = []
            vessel      = vessel
            currency    = 0.0<``Deca¥``>
        }

    let addMessage (turn:float<Turn>) (message:string) (avatar:Avatar) : Avatar =
        {avatar with messages = avatar.messages |> List.append [(turn, message)]}

    let getX (avatar:Avatar) : float<LightYear> =
        let (x,_,_) = avatar.location
        x

    let getY (avatar:Avatar) : float<LightYear> =
        let (_,y,_) = avatar.location
        y

    let getZ (avatar:Avatar) : float<LightYear> =
        let (_,_,z) = avatar.location
        z

    let getTheta (avatar:Avatar) : float<Degree> =
        avatar.heading
        |> fst

    let getPhi (avatar:Avatar) : float<Degree> =
        avatar.heading
        |> snd

    let clampSpeed (mininum:float<LightYear/Turn>) (maximum:float<LightYear/Turn>) (value:float<LightYear/Turn>): float<LightYear/Turn> =
        value
        |> max mininum
        |> min maximum

    let formatSpeed (value:float<LightYear/Turn>) : string = //TODO: this doesn't QUITE make sense here
        if value < 0.0<LightYear/Turn> then
            0.0<LightYear/Turn> |> abs |> sprintf "%5.2F"
        else
            value |> abs |> sprintf "%5.2F"


    let setHeading (logger:string->Avatar->Avatar) (heading: float<Degree> * float<Degree>) (avatar:Avatar) : Avatar =
        let theta =
            heading
            |> fst
            |> Degrees.clampTheta
        let phi =
            heading
            |> snd
            |> Degrees.clampPhi
        {avatar with heading = (theta, phi)}
        |> logger ((theta |> Degrees.format, phi |> Degrees.format) ||> sprintf "Heading set to %s mark %s.")

    let private setLocation (location: float<LightYear> * float<LightYear> * float<LightYear>) (avatar:Avatar) : Avatar =
        {avatar with location = location}

    let private getEnergyCostAndSpeed (avatar:Avatar) : float<``Megasthène``> * float<LightYear/Turn> =
        avatar.vessel
        |> Vessel.getEnergyCostAndSpeed

    let getSpeed = getEnergyCostAndSpeed >> snd

    let private transformVesselWithLogging (logger:string->Avatar->Avatar) (transform:Vessel->Vessel * (string list)) (avatar:Avatar) :Avatar =
        let result = avatar.vessel |> transform
        { avatar with
            vessel = result |> fst }
        |> List.foldBack
            (fun i s ->
                s |> logger i) (result |> snd)

    let private transformVessel (transform:Vessel->Vessel) (avatar:Avatar) :Avatar =
        {avatar with vessel = avatar.vessel |> transform}

    let private applyEnergyCost (cost:float<``Megasthène``>) =
        transformVessel (Vessel.applyEnergyCost cost)

    let move (logger:string->Avatar->Avatar) (avatar:Avatar) : Avatar =
        let costAndSpeed = avatar |> getEnergyCostAndSpeed
        let distance = (costAndSpeed |> snd) * 1.0<Turn>
        let newX = (avatar |> getX) + distance * cos(avatar |> getTheta |> Degrees.toRadians) * cos(avatar |> getPhi |> Degrees.toRadians)
        let newY = (avatar |> getY) + distance * sin(avatar |> getTheta |> Degrees.toRadians) * cos(avatar |> getPhi |> Degrees.toRadians)
        let newZ = (avatar |> getZ) + distance * sin(avatar |> getPhi |> Degrees.toRadians)
        avatar
        |> setLocation (newX, newY, newZ)
        |> applyEnergyCost (costAndSpeed |> fst)
        |> logger ( (newX |> Location.format, newY |> Location.format, newZ |> Location.format) |||> sprintf "Moved to (%s, %s, %s)." )

    let private statisticDefaultValues =
        [
            (Reputation, 0.0)
        ]
        |> Map.ofList

    let getStatistic (statistic:AvatarStatistic) (avatar:Avatar) : float =
        avatar.statistics
        |> Map.tryFind statistic
        |> Option.defaultValue statisticDefaultValues.[statistic]

    let private setStatistic (statistic:AvatarStatistic) (value:float) (avatar:Avatar) : Avatar =
        {avatar with statistics = avatar.statistics |> Map.add statistic value}

    let changeStatisticBy (logger:string->Avatar->Avatar) (statistic:AvatarStatistic) (amount:float) (avatar:Avatar) : Avatar =
        avatar
        |> setStatistic statistic ((avatar |> getStatistic statistic) + amount)
        |> logger ((statistic |> AvatarStatistic.name, amount |> AvatarStatistic.format statistic) ||> sprintf "Changed %s by %s.")

    let private setDelivery (delivery: Delivery option) (avatar:Avatar) : Avatar =
        {avatar with delivery = delivery}

    let abandonDelivery (logger:string->Avatar->Avatar) (avatar:Avatar) : Avatar =
        avatar
        |> Option.foldBack
            (fun _ a ->
                a
                |> changeStatisticBy logger Reputation (-1.0)
                |> setDelivery None) avatar.delivery

    let addVisits (howMany:int) (id:string) (avatar:Avatar) : Avatar =
        let previousVisitCount =
            avatar.visitCounts
            |> Map.tryFind id
            |> Option.defaultValue 0
        let updatedVisitCounts =
            avatar.visitCounts
            |> Map.add id (previousVisitCount + howMany)
        {avatar with visitCounts = updatedVisitCounts }

    let dock (logger:string->Avatar->Avatar) (id:string) (name:string) =
        addVisits 1 id
        >> logger (name |> sprintf "Docked at '%s'.")

    let acceptDelivery (logger:string->Avatar->Avatar) (delivery:Delivery) (name:string) (avatar: Avatar) : Avatar =
        avatar
        |> abandonDelivery logger
        |> addVisits 0 delivery.destination
        |> setDelivery (delivery |> Some)
        |> logger ((name, delivery.reward |> AvatarStatistic.formatCurrency) ||> sprintf "Accepted delivery mission to '%s' for %s.")

    let setCurrency (logger:string->Avatar->Avatar) (amount:float<``Deca¥``>) (avatar:Avatar) : Avatar =
        {avatar with currency = amount}

    let changeCurrencyBy (logger:string->Avatar->Avatar) (amount:float<``Deca¥``>) (avatar:Avatar) : Avatar =
        avatar
        |> setCurrency logger (avatar.currency + amount)

    let tryDeliver (logger:string->Avatar->Avatar) (id:string) (name:string) (avatar:Avatar) : Avatar =
        match avatar.delivery with
        | Some x when x.destination = id ->
            avatar
            |> setDelivery None
            |> logger (name |> sprintf "Delivered to '%s'.")
            |> changeCurrencyBy logger x.reward
            |> changeStatisticBy logger Reputation 1.0
        | _ ->
            avatar

    let applyTurns (turns:float<Turn>)=
        transformVessel (Vessel.applyTurns turns)

    let setThrottle (id:string) (throttle:float) =
        transformVessel (Vessel.setThrottle id throttle)

    let probe (logger:string->Avatar->Avatar) (location:Location) (avatar:Avatar) : Avatar =
        avatar
        |> transformVesselWithLogging logger (Vessel.probe (avatar.location |> Location.distance location))

    let addCommodityCargo (logger:string->Avatar->Avatar) (commodity:Commodity) (amount:float<Serplathe>) =
        transformVessel (Vessel.addCommodityCargo commodity amount)
        >> logger "Added cargo!"//TODO: better message

    let buyCommodity  (logger:string->Avatar->Avatar) (descriptor:CommodityDescriptor) (marketCommodity:MarketCommodity) (commodity:Commodity) (amount:float<Serplathe>) (avatar:Avatar) : Avatar =
        let totalPurchasePrice =
            (descriptor |> CommodityDescriptor.calculatePurchasePricePerUnit marketCommodity.supply marketCommodity.demand) * amount

        avatar
        |> changeCurrencyBy logger (-totalPurchasePrice)
        |> addCommodityCargo logger commodity amount

    let sellCommodity (descriptor:CommodityDescriptor) (marketCommodity:MarketCommodity) (commodity:Commodity) (amount:float<Serplathe>) (avatar:Avatar) : Avatar =
        avatar//TODO: work

    let getMaximumCommodityBuy (descriptor:CommodityDescriptor) (marketCommodity:MarketCommodity) (avatar:Avatar) : float<Serplathe> =
        let purchasePrice =
            descriptor
            |> CommodityDescriptor.calculatePurchasePricePerUnit marketCommodity.supply marketCommodity.demand
        avatar.currency / purchasePrice

    let getCargoSpaceAvailable (avatar:Avatar) : float<Serplathe> =
        avatar.vessel
        |> Vessel.getCargoSpaceAvailable
