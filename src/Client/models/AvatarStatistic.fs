namespace Splorr.Spacefarers

type AvatarStatistic =
    | Reputation

module AvatarStatistic =
    let name = function
        | Reputation -> "Reputation"


    let formatCurrency (value:float<``Deca¥``>) : string =
        if value < 0.0<``Deca¥``> then
            value |> abs |> sprintf "($%8.2F)"
        else
            value |> abs |> sprintf " $%8.2F "

    let private formatReputation (value:float) : string =
        if value < 0.0 then
            value |> abs |> sprintf "-%2.1F"
        else
            value |> abs |> sprintf "+%2.1F"

    let format = function
        | Reputation -> formatReputation
