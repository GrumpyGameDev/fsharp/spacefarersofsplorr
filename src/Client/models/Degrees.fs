namespace Splorr.Spacefarers

module Degrees =
    let toRadians (degrees: float<Degree>) : float =
        degrees / 180.0<Degree> * System.Math.PI

    let fromRadians (radians:float) : float<Degree> =
        radians * 180.0<Degree> / System.Math.PI

    let clampTheta =
        max -180.0<Degree> >> min 180.0<Degree>

    let clampPhi =
        max -90.0<Degree> >> min 90.0<Degree>

    let format (degrees:float<Degree>) : string =
        if degrees<0.0<Degree> then
            degrees |> abs |> sprintf "-%07.3F"
        else
            degrees |> abs |> sprintf "+%07.3F"
