module Client

open Elmish
open Elmish.React
open Splorr.Spacefarers

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram Init.init Update.update View.view
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactBatched "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
