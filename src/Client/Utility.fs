namespace Splorr.Spacefarers

module Utility =
    let generateFloat (random:System.Random) (minimum:float) (maximum:float) : float =
        random.NextDouble() * (maximum - minimum) + minimum