namespace Splorr.Spacefarers

open Fulma

module MainMenuView =
    let content (dispatch : Msg -> unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Main Menu"

                Columns.columns
                    []
                    [
                        Column.column
                            []
                            [ ViewUtility.button None "Start Game" (fun _ -> MainMenuMsg.StartGame |> Msg.MainMenu |> dispatch) ]
                        Column.column
                            []
                            [ ViewUtility.button None "Instructions" (fun _ -> ShowInstructions |> MainMenu |> dispatch) ]
                        Column.column
                            []
                            [ ViewUtility.button None "About" (fun _ -> ShowAbout |> MainMenu |> dispatch) ]
                    ]
            ]
