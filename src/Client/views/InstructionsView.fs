namespace Splorr.Spacefarers

open Fulma
open Fable.React

module InstructionsView =
    let content (dispatch : Msg -> unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "How To Play"

                Content.content [] [ str "TODO: put how to play information in here" ]

                ViewUtility.button None "Main Menu" (fun _ -> dispatch ShowMainMenu)
            ]

