namespace Splorr.Spacefarers

open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props
open Fetch.Types
open Thoth.Fetch
open Fulma
open Thoth.Json

module ViewUtility =
    let safeComponents =
        span
            [ ]
            [
                str "Version "
                strong
                    [ ]
                    [ str Version.app ]
            ]

    let button (color:IColor option) txt onClick =
        Button.button
            [
                Button.IsFullWidth
                Button.Color (color |> Option.defaultValue IsPrimary)
                Button.OnClick onClick
            ]
            [
                str txt
            ]

    let navbar =
        Navbar.navbar
            [ Navbar.Color IsPrimary ]
            [
                Navbar.Item.div
                    [ ]
                    [ Heading.h2
                        [ ]
                        [ str "Spacefarers of Splorr!!" ]
                    ]
            ]

    let modelStateHeader (text:string) =
        Content.content
            [
                Content.Modifiers
                    [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ]
            ]
            [
                Heading.h3
                    []
                    [ str text ]
            ]

    let footer =
        Footer.footer
            [ ]
            [
                Content.content
                    [
                        Content.Modifiers
                            [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ]
                    ]
                    [ safeComponents ]
            ]



