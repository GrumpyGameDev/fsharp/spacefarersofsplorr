namespace Splorr.Spacefarers

open Fable.React
open Fulma

module InGameView =
    let content (game:Game) (dispatch : Msg -> unit) =
        match game.state with
        | SetHeading (theta, phi) ->
            SetHeadingView.content (theta, phi) game dispatch
        | SetDestination _ ->
            SetDestinationView.content game dispatch
        | Docked (id,state) ->
            DockedView.content id game.entities.[id] state game dispatch
        | Status _ ->
            StatusView.content game dispatch
        | VesselStatus _ ->
            VesselStatusView.content game dispatch
        | SetThrottle (id, throttle, state) ->
            SetThrottleView.content id throttle game dispatch
        | EntityStatus (id, state) ->
            EntityStatusView.content id game dispatch
        | _ ->
            InSpaceView.content game dispatch