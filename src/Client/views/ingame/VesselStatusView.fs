namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module VesselStatusView =
    let private fuelSupplyPanelBlocks (headerPanel:ReactElement) (fuelSupply:FuelSupply) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Capacity: " ]
                    fuelSupply.maximum |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Current Level: " ]
                    fuelSupply.current |> sprintf "%f" |> str
                ]
        ]

    let private passiveSensorPanelBlocks (headerPanel:ReactElement) (passiveSensor:PassiveSensor) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Range: " ]
                    passiveSensor.range |> sprintf "%f" |> str
                ]
        ]

    let private probingSensorPanelBlocks (headerPanel:ReactElement) (passiveSensor:ProbingSensor) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Range: " ]
                    passiveSensor.range |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Squared): " ]
                    passiveSensor.conversion.squared |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Linear): " ]
                    passiveSensor.conversion.linear |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Constant): " ]
                    passiveSensor.conversion.constant |> sprintf "%f" |> str
                ]
        ]

    let private reactorPanelBlocks (id:string) (headerPanel:ReactElement) (dispatch:Msg->unit) (reactor:Reactor) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Capacity: " ]
                    reactor.maximumInput |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Throttle: " ]
                    reactor.throttle |> sprintf "%f" |> str
                    Button.button
                        [
                            Button.Color IsPrimary
                            Button.OnClick (fun _ -> (id, ChangeOrderMsg.Begin) |> ChangeThrottle |> InGame |> dispatch )
                        ]
                        [ str "Change..." ]
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Squared): " ]
                    reactor.conversion.squared |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Linear): " ]
                    reactor.conversion.linear |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Constant): " ]
                    reactor.conversion.constant |> sprintf "%f" |> str
                ]
        ]

    let private batteryPanelBlocks (headerPanel:ReactElement) (battery:Battery) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Capacity: " ]
                    battery.maximum |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Current Level: " ]
                    battery.current |> sprintf "%f" |> str
                ]
        ]

    let private cargoHoldPanelBlocks (headerPanel:ReactElement) (cargoHold:CargoHold) =
        cargoHold.cargo
        |> Map.fold
            (fun a k v ->
                [ div [ classList [("panel-block",true) ] ]
                    [
                        strong [] [ (k.ToString()) |> sprintf "%s:" |> str ]
                        v |> sprintf "%f" |> str
                    ] ]
                |> List.append a) []
        |> List.append
            [
                headerPanel

                div [ classList [("panel-block",true) ] ]
                    [
                        strong [] [ str "Capacity: " ]
                        cargoHold.capacity |> sprintf "%f" |> str
                    ]
            ]

    let private drivePanelBlocks (id:string) (headerPanel:ReactElement) (dispatch:Msg->unit) (drive:Drive) =
        [
            headerPanel

            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Maximum Speed: " ]
                    drive.maximumSpeed |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Throttle: " ]
                    drive.throttle |> sprintf "%f" |> str
                    Button.button
                        [
                            Button.Color IsPrimary
                            Button.OnClick (fun _ -> (id, ChangeOrderMsg.Begin) |> ChangeThrottle |> InGame |> dispatch )
                        ]
                        [ str "Change..." ]
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Squared): " ]
                    drive.conversion.squared |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Linear): " ]
                    drive.conversion.linear |> sprintf "%f" |> str
                ]
            div [ classList [("panel-block",true) ] ]
                [
                    strong [] [ str "Conversion (Constant): " ]
                    drive.conversion.constant |> sprintf "%f" |> str
                ]
        ]

    let private componentPanelBlocks (id:string) (dispatch:Msg->unit) (headerPanel:ReactElement) (``component``:VesselComponent) =
        match ``component`` with
        | FuelSupply fs ->
            fs |> fuelSupplyPanelBlocks headerPanel
        | Reactor r ->
            r |> reactorPanelBlocks id headerPanel dispatch
        | Battery b ->
            b |> batteryPanelBlocks headerPanel
        | Drive d ->
            d |> drivePanelBlocks id headerPanel dispatch
        | PassiveSensor p ->
            p |> passiveSensorPanelBlocks headerPanel
        | ProbingSensor p ->
            p |> probingSensorPanelBlocks headerPanel
        | CargoHold c ->
            c |> cargoHoldPanelBlocks headerPanel

    let private componentPanel (dispatch:Msg->unit) (id:string) (``component``: VesselComponent) =
        let headerPanel =
            div
                [classList [("panel-heading",true)] ]
                [ (``component`` |> VesselComponent.componentTypeName, id) ||> sprintf "%s (%s)" |> str ]

        let panelBlocks =
            componentPanelBlocks id dispatch headerPanel ``component``

        Panel.panel
            []
            panelBlocks

    let content (game:Game) (dispatch:Msg->unit) =
        let buttonColumns : ReactElement list =
            [ ]

        let equipment =
            game.avatar.vessel.components
            |> Map.map
                (componentPanel dispatch)
            |> Map.toList
            |> List.map snd

        let goBackTabText =
            match game.state with
            | VesselStatus InSpace ->
                "Helm"
            | VesselStatus (Docked _) ->
                "Dock"
            | _ ->
                "Go Back"

        Container.container
            []
            [
                InGameViewUtility.standardTabs goBackTabText VesselStatusTab dispatch

                Content.content []
                    equipment

                Columns.columns
                    []
                    buttonColumns
            ]
