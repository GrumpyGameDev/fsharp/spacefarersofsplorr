namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module StatusView =
    let content (game:Game) (dispatch:Msg->unit) =
        let logEntries : ReactElement list =
            game.avatar.messages
            |> List.truncate 100
            |> List.map (fun message ->
                tr []
                    [
                        td [] [ message |> fst |> Game.formatTurns |> str ]
                        td [] [ message |> snd |> str ]
                    ]
                )

        let buttonColumns : ReactElement list =
            [ ]
            |> Option.foldBack
                (fun _ s ->
                    [
                        Column.column
                            []
                            [
                                ViewUtility.button (IsDanger |> Some) "Abandon Delivery" (fun _ -> AbandonDelivery |> InGame |> dispatch)
                            ]
                    ]
                    |> List.append s) game.avatar.delivery

        let levelItems : ReactElement list =
            [
                Level.item
                    [
                        Level.Item.HasTextCentered
                    ]
                    [
                        div
                            []
                            [
                                Level.heading [] [ str "Turn" ]
                                Level.title [] [ game.turnCount |> sprintf "%08.2f" |> str]
                            ]
                    ]
                Level.item
                    [
                        Level.Item.HasTextCentered
                    ]
                    [
                        div
                            []
                            [
                                Level.heading [] [ str "Currency" ]
                                Level.title [] [game.avatar.currency |> AvatarStatistic.formatCurrency |> str]
                            ]
                    ]
                Level.item
                    [
                        Level.Item.HasTextCentered
                    ]
                    [
                        div
                            []
                            [
                                Level.heading [] [ str "Reputation" ]
                                Level.title [] [game.avatar |> Avatar.getStatistic Reputation |> sprintf "%07.3f" |> str]
                            ]
                    ]
            ]
            |> Option.foldBack
                (fun d s ->
                    let entity = game.entities.[d.destination]
                    [
                        Level.item
                            [
                                Level.Item.HasTextCentered
                            ]
                            [
                                div
                                    []
                                    [
                                        Level.heading [] [ str "Delivery" ]
                                        Level.title []
                                            [
                                                entity |> Entity.getName |> str
                                                br []
                                                d.reward |> sprintf "%f" |> str
                                            ]
                                    ]
                            ]
                    ]
                    |> List.append s) game.avatar.delivery

        let goBackTabText =
            match game.state with
            | Status InSpace ->
                "Helm"
            | Status (Docked _) ->
                "Dock"
            | _ ->
                "Go Back"

        Container.container
            []
            [
                InGameViewUtility.standardTabs goBackTabText StatusTab dispatch

                Level.level
                    []
                    levelItems

                Columns.columns
                    []
                    buttonColumns

                Table.table [ Table.IsFullWidth ]
                    [
                        thead []
                            [
                                tr []
                                    [
                                        th [] [ str "Turn" ]
                                        th [] [ str "Log Entry" ]
                                    ]
                            ]
                        tbody []
                            logEntries
                    ]

            ]
