namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module SetDestinationView =
    let content (game:Game) (dispatch:Msg->unit) =
        let entities =
            game
            |> Game.getKnownEntities
            |> List.sortBy (fun (_,(e,_)) -> e |> Entity.getName)
            |> List.map
                (fun (id, (entity, canProbe)) -> InGameViewUtility.showEntity false (fun _ -> id |> Some |> ChangeOrderMsg.Issue |> SelectDestination |> InGame |> dispatch) id entity canProbe game.avatar dispatch)

        Container.container []
            [
                ViewUtility.modelStateHeader "Set Destination"

                InGameViewUtility.showEntitiesTable entities

                Columns.columns
                    []
                    [
                        Column.column
                            []
                            [ ViewUtility.button None "Belay That!" (fun _ -> ChangeOrderMsg.Belay |> SelectDestination |> InGame |> dispatch) ]
                    ]

            ]
