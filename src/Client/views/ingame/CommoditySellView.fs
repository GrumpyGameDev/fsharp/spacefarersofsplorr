namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module CommoditySellView =
    let content (id:string) (starPort:StarPort) (commodity:Commodity) (amount:float<Serplathe>) (game:Game) (dispatch:Msg->unit) =
        let buttonColumns =
            [
                Column.column []
                    [ ViewUtility.button None "Exit Market" (fun _ -> (commodity, ChangeOrderMsg.Belay) |> SellCommodity |> InGame |> dispatch) ]
            ]

        Container.container []
            [
                ViewUtility.modelStateHeader "Sell"

                Columns.columns []
                    buttonColumns

            ]
