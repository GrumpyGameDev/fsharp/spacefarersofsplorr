namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module CommodityBuyView =
    let content (id:string) (starPort:StarPort) (commodity:Commodity) (amount:float<Serplathe>) (game:Game) (dispatch:Msg->unit) =
        let descriptor = game.commodities.[commodity]
        let marketCommodity = game |> Game.getMarketCommodity id commodity |> Option.get
        let purchasePrice =
            game.commodities.[commodity]
            |> CommodityDescriptor.calculateUnitPurchasePrice marketCommodity.supply marketCommodity.demand
        let maximumBuy =
            (game.avatar |> Avatar.getCargoSpaceAvailable)
            |> min (game.avatar |> Avatar.getMaximumCommodityBuy descriptor marketCommodity)

        let buttonColumns =
            [
                Column.column []
                    [ ViewUtility.button None "Buy" (fun _ -> (descriptor, marketCommodity, commodity, ChangeOrderMsg.Issue amount) |> BuyCommodity |> InGame |> dispatch) ]
                Column.column []
                    [ ViewUtility.button None "Buy Maximum" (fun _ -> (descriptor, marketCommodity, commodity, ChangeOrderMsg.Issue maximumBuy) |> BuyCommodity |> InGame |> dispatch) ]
                Column.column []
                    [ ViewUtility.button None "Exit Market" (fun _ -> (descriptor, marketCommodity, commodity, ChangeOrderMsg.Belay) |> BuyCommodity |> InGame |> dispatch) ]
            ]

        Container.container []
            [
                ViewUtility.modelStateHeader "Buy"

                p []
                    [
                        strong []
                            [ "Commodity: " |> str ]

                        descriptor.name |> str
                    ]

                p []
                    [
                        strong []
                            [ "Price Per Serplathe: " |> str ]

                        purchasePrice |> AvatarStatistic.formatCurrency |> str
                    ]

                p []
                    [
                        strong []
                            [ "Maximum Buy: " |> str ]

                        maximumBuy |> sprintf "%f" |> str//TODO: format serplathes better
                    ]

                Content.content
                    []
                    [
                        Label.label []
                            [
                                str "Buy Amount:"
                                Input.number
                                    [
                                        Input.OnChange
                                            (fun event ->
                                                match event.Value |> System.Double.TryParse with
                                                | true, s ->
                                                    (descriptor, marketCommodity, commodity, (s * 1.0<Serplathe>) |> Change) |> BuyCommodity |> InGame |> dispatch
                                                | _ ->
                                                    ())
                                        Input.DefaultValue (amount |> sprintf "%f")
                                    ]
                            ]
                    ]

                Columns.columns []
                    buttonColumns

            ]
