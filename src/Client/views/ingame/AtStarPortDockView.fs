namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module AtStarPortDockView =
    let content (starPort:StarPort) (game:Game) (dispatch:Msg->unit) =
        let availableDelivery =
            game
            |> Game.getAvailableDelivery

        let buttonColumns =
            [
                Column.column []
                    [ ViewUtility.button None "Get Underway" (fun _ -> Undock |> InGame |> dispatch ) ]
            ]
            |> Option.foldBack
                (fun _ columns ->
                    [
                        Column.column []
                            [ ViewUtility.button None "Delivery..." (fun _ -> Begin |> AcceptDelivery |> InGame |> dispatch) ]
                    ]
                    |> List.append columns) availableDelivery

        let supplyAndDemand =
            starPort.market
            |> Map.map
                (fun k v ->
                    tr []
                        [
                            td []
                                [
                                    game.commodities.[k].name |> str
                                ]
                            td []
                                [
                                    CommodityDescriptor.calculateUnitSalePrice v.supply v.demand game.commodities.[k]
                                    |> AvatarStatistic.formatCurrency
                                    |> str

                                    str " ("

                                    CommodityDescriptor.calculateUnitPurchasePrice v.supply v.demand game.commodities.[k]
                                    |> AvatarStatistic.formatCurrency
                                    |> str

                                    str ")"

                                ]
                            td []
                                [
                                    Button.button
                                        [
                                            Button.OnClick (fun _ -> (game.commodities.[k], v, k, ChangeOrderMsg.Begin) |> BuyCommodity |> InGame |> dispatch )
                                        ]
                                        [ str "Buy" ]
                                    Button.button
                                        [
                                            Button.OnClick (fun _ -> (k, ChangeOrderMsg.Begin) |> SellCommodity |> InGame |> dispatch )
                                        ]
                                        [ str "Sell" ]
                                ]
                        ])
            |> Map.toList
            |> List.map snd

        Container.container []
            [
                InGameViewUtility.standardTabs "Dock" GoBackTab dispatch

                p []
                    [
                        strong [] [ str "Star Port: " ]
                        str starPort.name
                    ]

                Table.table [ Table.IsFullWidth ]
                    [
                        thead []
                            [
                                tr []
                                    [
                                        th [] [ str "Commodity" ]
                                        th [] [ str "Price" ]
                                        th [] [ str "Actions" ]
                                    ]
                            ]
                        tbody []
                            supplyAndDemand
                    ]

                Columns.columns []
                    buttonColumns

            ]
