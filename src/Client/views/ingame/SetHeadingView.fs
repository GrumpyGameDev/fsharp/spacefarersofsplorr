namespace Splorr.Spacefarers

open Fable.React
open Fulma

module SetHeadingView =
    let content (theta:float<Degree>, phi: float<Degree>) (game:Game) (dispatch:Msg->unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Set Speed"

                Content.content
                    []
                    [
                        Label.label [] [ str "New Theta:" ]
                        Input.number
                            [
                                Input.OnChange
                                    (fun event ->
                                        match event.Value |> System.Double.TryParse with
                                        | true, s ->
                                            (s * 1.0<Degree>,phi) |> Change |> ChangeHeading |> InGame |> dispatch
                                        | _ ->
                                            ())
                                Input.DefaultValue (theta |> sprintf "%f")
                            ]
                    ]

                Content.content
                    []
                    [
                        Label.label [] [ str "New Phi:" ]
                        Input.number
                            [
                                Input.OnChange
                                    (fun event ->
                                        match event.Value |> System.Double.TryParse with
                                        | true, s ->
                                            (theta,s * 1.0<Degree>) |> Change |> ChangeHeading |> InGame |> dispatch
                                        | _ ->
                                            ())
                                Input.DefaultValue (phi |> sprintf "%f")
                            ]
                    ]

                Columns.columns
                    []
                    [
                        Column.column
                            []
                            [ ViewUtility.button None "Make It So!" (fun _ -> (theta, phi) |> ChangeOrderMsg.Issue |> ChangeHeading |> InGame |> dispatch) ]
                        Column.column
                            []
                            [ ViewUtility.button None "Belay That!" (fun _ -> ChangeOrderMsg.Belay |> ChangeHeading |> InGame |> dispatch) ]
                    ]

            ]
