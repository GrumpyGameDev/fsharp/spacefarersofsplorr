namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

module InSpaceView =
    let content (game:Game) (dispatch:Msg->unit) =
        let headForButton =
            if game.avatar.visitCounts.IsEmpty then
                None
            else
                Column.column []
                    [ ViewUtility.button None "Head For..." (fun _ -> ChangeOrderMsg.Begin |> SelectDestination |> InGame |> dispatch)]
                |> Some

        let buttonColumns =
            [
                Column.column []
                    [ ViewUtility.button None "Move" (fun _ -> Move |> InGame |> dispatch) ]

                Column.column []
                    [ ViewUtility.button None "Set Heading..." (fun _ -> ChangeOrderMsg.Begin |> ChangeHeading |> InGame |> dispatch) ]
            ]
            |> Option.foldBack
                (fun i s ->
                    [i]
                    |> List.append s) headForButton

        Container.container
            []
            [
                InGameViewUtility.standardTabs "Helm" GoBackTab dispatch

                InGameViewUtility.showAvatarState game.avatar dispatch

                Columns.columns []
                    buttonColumns

                InGameViewUtility.showNearbyEntities game dispatch
            ]
