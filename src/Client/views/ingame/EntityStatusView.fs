namespace Splorr.Spacefarers

open Fable.React
open Fulma

module EntityStatusView =
    let content (id:string) (game:Game) (dispatch:Msg->unit) =
        let entity = game.entities.[id]
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Entity Status"

                p []
                    [
                        strong [] [ str "Name: " ]
                        str (entity |> Entity.getName)
                    ]

                Content.content
                    []
                    [
                    ]

                Columns.columns
                    []
                    [
                        Column.column
                            []
                            [ ViewUtility.button None "Go Back" (fun _ -> GoBack |> InGame |> dispatch) ]
                    ]

            ]
