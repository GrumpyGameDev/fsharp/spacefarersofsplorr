namespace Splorr.Spacefarers

open Fable.React
open Fulma

module DockedView =
    let content (id:string) (entity:Entity) (dockedState:DockedState) (game:Game) (dispatch:Msg->unit) =
        match entity.entityType, dockedState with
        | StarPort starPort, AtDock ->
            AtStarPortDockView.content starPort game dispatch
        | StarPort starPort, CommodityBuy (commodity, amount) ->
            CommodityBuyView.content id starPort commodity amount game dispatch
        | StarPort starPort, CommoditySell (commodity, amount) ->
            CommoditySellView.content id starPort commodity amount game dispatch
        | _, PendingDelivery ->
            PendingDeliveryView.content (game |> Game.getAvailableDelivery |> Option.get) game dispatch
