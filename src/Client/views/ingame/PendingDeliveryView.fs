namespace Splorr.Spacefarers

open Fable.React
open Fulma

module PendingDeliveryView =
    let content (delivery:Delivery) (game:Game) (dispatch:Msg->unit) =
        let entity =
            game.entities.[delivery.destination]
        let buttonColumns =
            [
                Column.column []
                    [ ViewUtility.button (game.avatar.delivery |> Option.map (fun _ -> IsDanger)) "Accept" (fun _ -> Issue |> AcceptDelivery |> InGame |> dispatch) ]

                Column.column []
                    [ ViewUtility.button None "Never Mind" (fun _ -> Belay |> AcceptDelivery |> InGame |> dispatch ) ]
            ]

        Container.container []
            [
                ViewUtility.modelStateHeader "Accept Delivery?"

                Content.content []
                    [
                        p []
                            [
                                strong [] [ str "Destination: "]
                                entity |> Entity.getName |> str
                            ]
                        p []
                            [
                                strong [] [ str "Reward: "]
                                delivery.reward |> sprintf "%f" |> str
                            ]
                        p []
                            [
                                strong [] [ str "Distance: "]
                                (game.avatar.location, game.entities.[delivery.destination].location)
                                ||> Location.distance
                                |> Location.format
                                |> str
                            ]
                    ]

                Columns.columns []
                    buttonColumns

            ]
