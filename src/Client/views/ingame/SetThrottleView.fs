namespace Splorr.Spacefarers

open Fable.React
open Fulma

module SetThrottleView =
    let content (id:string) (throttle:float) (game:Game) (dispatch:Msg->unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Set Throttle"

                Content.content
                    []
                    [
                        Label.label [] [ str "New Throttle:" ]
                        Input.number
                            [
                                Input.OnChange
                                    (fun event ->
                                        match event.Value |> System.Double.TryParse with
                                        | true, s ->
                                            (id, s |> Change) |> ChangeThrottle |> InGame |> dispatch
                                        | _ ->
                                            ())
                                Input.DefaultValue (throttle |> sprintf "%f")
                            ]
                    ]

                Columns.columns
                    []
                    [
                        Column.column
                            []
                            [ ViewUtility.button None "Make It So!" (fun _ -> (id, throttle |> ChangeOrderMsg.Issue) |> ChangeThrottle |> InGame |> dispatch) ]
                        Column.column
                            []
                            [ ViewUtility.button None "Belay That!" (fun _ -> (id, ChangeOrderMsg.Belay) |> ChangeThrottle |> InGame |> dispatch) ]
                    ]

            ]
