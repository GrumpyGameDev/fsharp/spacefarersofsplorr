namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma

type TabIdentifier =
    | GoBackTab
    | StatusTab
    | VesselStatusTab

module InGameViewUtility =
    let standardTabs (goBackText:string) (tabIdentifier:TabIdentifier) (dispatch:Msg->unit) =
        Tabs.tabs
            [
                Tabs.IsCentered
            ]
            [
                li [ classList [("is-active",tabIdentifier = GoBackTab)] ]
                    [ a
                        [ OnClick (fun _ -> GoBack |> InGame |> dispatch) ]
                        [ str goBackText] ]
                li [ classList [("is-active",tabIdentifier = StatusTab)] ]
                    [ a
                        [ OnClick (fun _ -> ViewStatus |> InGame |> dispatch) ]
                        [ str "Status"] ]
                li [ classList [("is-active",tabIdentifier = VesselStatusTab)] ]
                    [ a
                        [ OnClick (fun _ -> ViewVesselStatus |> InGame |> dispatch) ]
                        [ str "Vessel"] ]
            ]


    let showAvatarState (avatar:Avatar) (dispatch:Msg->unit) =
        Content.content
            []
            [
                Level.level []
                    [
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "X" ]
                                        Level.title [] [avatar |> Avatar.getX |> Location.format |> str]
                                    ]
                            ]
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "Y" ]
                                        Level.title [] [avatar |> Avatar.getY |> Location.format |> str]
                                    ]
                            ]
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "Z" ]
                                        Level.title [] [avatar |> Avatar.getZ |> Location.format |> str]
                                    ]
                            ]
                    ]
                Level.level []
                    [
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "Speed" ]
                                        Level.title [] [avatar |> Avatar.getSpeed |> Avatar.formatSpeed |> str]
                                    ]
                            ]
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "Theta" ]
                                        Level.title [] [avatar |> Avatar.getTheta |> Degrees.format |> str]
                                    ]
                            ]
                        Level.item [ Level.Item.HasTextCentered ]
                            [
                                div []
                                    [
                                        Level.heading [] [ str "Phi" ]
                                        Level.title [] [avatar |> Avatar.getPhi |> Degrees.format |> str]
                                    ]
                            ]
                    ]
            ]

    let showEntity (canDock:bool) (headForCommand:Browser.Types.MouseEvent->unit) (id:string) (entity:Entity) (canProbe:bool) (avatar:Avatar)  (dispatch:Msg->unit) =
        let typeName, needsProbe =
            match avatar.visitCounts |> Map.tryFind id, entity.entityType with
            | Some _, StarPort _ -> ("Star Port" |> Some), false
            | None, _ -> None, true
        let name =
            match avatar.visitCounts |> Map.tryFind id, entity.entityType with
            | Some _, StarPort s ->
                avatar.delivery
                |> Option.fold
                    (fun acc i ->
                        if i.destination = id then
                            [
                                a
                                    [ OnClick (fun _ -> id |> ViewEntityStatus |> InGame |> dispatch) ]
                                    [ s.name |> str ]
                                Tag.tag
                                    [ Tag.Color IsSuccess ]
                                    [ str "Delivery!" ]
                            ]
                        else
                            acc) [ a
                                    [ OnClick (fun _ -> id |> ViewEntityStatus |> InGame |> dispatch) ]
                                    [ s.name |> str ] ]
            | None, _ -> [ str "??????" ]
        let distance =
            entity.location
            |> Location.distance avatar.location
        let actions : ReactElement list =
            (if needsProbe && canProbe then
                [
                    Button.button
                        [ Button.IsFullWidth; Button.Color IsInfo; Button.OnClick (fun _ -> id |> Probe |> InGame |> dispatch) ]
                        [ str "Probe" ]
                ]
            else
                [])
            |> List.append
                [
                    (if canDock && entity |> Entity.canDock avatar then
                        Button.button
                            [ Button.IsFullWidth; Button.Color IsSuccess; Button.OnClick (fun _ -> id |> Dock |> InGame |> dispatch) ]
                            [ str "Dock" ]
                    else
                        Button.button
                            [ Button.IsFullWidth; Button.OnClick headForCommand ]
                            [ str "Head For"])
                ]

        tr []
            [
                td []
                    (name
                    |> Option.foldBack
                        (fun i s ->
                            [
                                Tag.tag
                                    [ Tag.Color IsInfo ]
                                    [ str i ]
                            ]
                            |> List.append s) typeName)
                td []
                    [
                        distance |> Location.format |> str
                    ]
                td []
                    [
                        entity.location
                        |> Location.thetaTo avatar.location
                        |> Degrees.format
                        |> str

                        " mark " |> str

                        entity.location
                        |> Location.phiTo avatar.location
                        |> Degrees.format
                        |> str
                    ]
                td []
                    actions

            ]

    let showEntitiesTable (entities:ReactElement list) =
        Table.table [ Table.IsFullWidth ]
            [
                thead []
                    [
                        tr []
                            [
                                th [] [ str "Entity" ]
                                th [] [ str "Distance" ]
                                th [] [ str "Heading" ]
                                th [] [ str "Actions"]
                            ]
                    ]
                tbody []
                    entities
            ]

    let showNearbyEntities (game:Game) (dispatch:Msg->unit) =
        let nearbyEntities =
            game
            |> Game.getNearbyEntities
            |> List.sortBy (fun (_,(e,_)) -> game.avatar.location |> Location.distance e.location)
            |> List.map
                (fun (id, (entity, canProbe)) -> showEntity true (fun _ -> id |> HeadFor |> InGame |> dispatch) id entity canProbe game.avatar dispatch)
        showEntitiesTable nearbyEntities
