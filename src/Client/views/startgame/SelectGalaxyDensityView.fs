namespace Splorr.Spacefarers

open Fulma
open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props

module SelectGalaxyDensityView =
    let content (dispatch : Msg -> unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Galaxy Density"

                Content.content
                    []
                    [
                        ViewUtility.button None "Dense (7.5LY Minimum Distance)" (fun _ -> 7.5<LightYear> |> SetGalaxyDensity |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "Normal (10LY Minimum Distance)" (fun _ -> 10.0<LightYear> |> SetGalaxyDensity |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "Sparse (15LY Minimum Distance)" (fun _ -> 15.0<LightYear> |> SetGalaxyDensity |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "<- Back" (fun _ -> dispatch ShowMainMenu)
                    ]
            ]
