namespace Splorr.Spacefarers

open Fulma
open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props

module SelectGalaxySizeView =
    let content (dispatch : Msg -> unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "Galaxy Size"

                Content.content
                    []
                    [
                        ViewUtility.button None "Small (50LY x 50LY x 50LY)" (fun _ -> (50.0<LightYear>,50.0<LightYear>,50.0<LightYear>) |> SetGalaxySize |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "Medium (75LY x 75LY x 75LY)" (fun _ -> (75.0<LightYear>,75.0<LightYear>,75.0<LightYear>) |> SetGalaxySize |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "Large (100LY x 100LY x 100LY)" (fun _ -> (100.0<LightYear>,100.0<LightYear>,100.0<LightYear>) |> SetGalaxySize |> StartGame |> dispatch)
                    ]

                Content.content
                    []
                    [
                        ViewUtility.button None "<- Back" (fun _ -> dispatch ShowMainMenu)
                    ]
            ]
