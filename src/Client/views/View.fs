namespace Splorr.Spacefarers

open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props
open Fetch.Types
open Thoth.Fetch
open Fulma
open Thoth.Json

module View =

    let private content (model : Model) (dispatch : Msg -> unit) =
        match model with
        | Model.MainMenu ->
            MainMenuView.content dispatch
        | Instructions ->
            InstructionsView.content dispatch
        | Model.About ->
            AboutView.content dispatch
        | NewGame (None, _)->
            SelectGalaxySizeView.content dispatch
        | NewGame _ ->
            SelectGalaxyDensityView.content dispatch
        | Model.InGame game ->
            InGameView.content game dispatch

    let view (model : Model) (dispatch : Msg -> unit) =
        div []
            [
                ViewUtility.navbar

                content model dispatch

                ViewUtility.footer
            ]
