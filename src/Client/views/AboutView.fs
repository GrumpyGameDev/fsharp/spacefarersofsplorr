namespace Splorr.Spacefarers

open Fable.React
open Fable.React.Props
open Fulma


module AboutView =
    let content (dispatch : Msg -> unit) =
        Container.container
            []
            [
                ViewUtility.modelStateHeader "About"

                p []
                    [
                        str "Created by"
                        a
                            [
                                Href "https://www.twitch.tv/thegrumpygamedev"
                            ]
                            [
                                str "TheGrumpyGameDev"
                            ]
                    ]

                p []
                    [
                        a
                            [
                                Href "https://gitlab.com/GrumpyGameDev/fsharp/spacefarersofsplorr"
                            ]
                            [
                                str "GitLab Repository"
                            ]
                    ]

                p []
                    [
                        str "Service Desk Email: incoming+grumpygamedev-fsharp-spacefarersofsplorr-14827801-issue-@incoming.gitlab.com"
                    ]

                ViewUtility.button None "Main Menu" (fun _ -> dispatch ShowMainMenu)
            ]

