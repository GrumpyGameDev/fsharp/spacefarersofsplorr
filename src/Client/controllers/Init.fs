namespace Splorr.Spacefarers

open Elmish

module Init =
    let init () : Model * Cmd<Msg> =
        Model.MainMenu, Cmd.none
