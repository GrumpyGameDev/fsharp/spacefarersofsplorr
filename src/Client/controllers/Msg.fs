namespace Splorr.Spacefarers

type MainMenuMsg =
    | StartGame
    | ShowInstructions
    | ShowAbout

type StartGameMsg =
    | SetGalaxySize of float<LightYear> * float<LightYear> * float<LightYear>
    | SetGalaxyDensity of float<LightYear>

type ChangeOrderMsg<'T> =
    | Begin
    | Change of 'T
    | Issue of 'T
    | Belay

type AcceptOrderMsg =
    | Begin
    | Issue
    | Belay

type InGameMsg =
    | Move
    | ChangeHeading of ChangeOrderMsg<float<Degree> * float<Degree>>
    | ChangeThrottle of string * ChangeOrderMsg<float>
    | HeadFor of string
    | Dock of string
    | Undock
    | SelectDestination of ChangeOrderMsg<string option>
    | ViewStatus
    | ViewVesselStatus
    | ViewEntityStatus of string
    | GoBack
    | AbandonDelivery
    | AcceptDelivery of AcceptOrderMsg
    | Wait
    | Probe of string
    | BuyCommodity of CommodityDescriptor * MarketCommodity * Commodity * ChangeOrderMsg<float<Serplathe>>
    | SellCommodity of Commodity * ChangeOrderMsg<float<Serplathe>>

type Msg =
    | MainMenu of MainMenuMsg
    | ShowMainMenu
    | StartGame of StartGameMsg
    | InGame of InGameMsg

