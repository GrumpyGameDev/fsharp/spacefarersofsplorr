namespace Splorr.Spacefarers

open Elmish

module MainMenuUpdate =
    let update (msg:MainMenuMsg) : Model * Cmd<Msg> =
        match msg with
        | MainMenuMsg.StartGame ->
            (NewGame (None, None)), Cmd.none

        | ShowInstructions ->
            Instructions, Cmd.none

        | ShowAbout ->
            About, Cmd.none