namespace Splorr.Spacefarers

open Elmish

module StartGameUpdate =
    let update (msg:StartGameMsg) (currentModel : Model) : Model * Cmd<Msg> =
        match msg with
        | SetGalaxySize (x,y,z) ->
            match currentModel with
            | NewGame (_, Some d) ->
                (Game.create (x,y,z) d |> Model.InGame), Cmd.none
            | NewGame _ ->
                (NewGame (Some (x,y,z),None)), Cmd.none
            | _ ->
                currentModel, Cmd.none
        | SetGalaxyDensity d ->
            match currentModel with
            | NewGame (Some s, _) ->
                (Game.create s d |> Model.InGame), Cmd.none
            | NewGame _ ->
                (NewGame (None, None)), Cmd.none
            | _ ->
                currentModel, Cmd.none
