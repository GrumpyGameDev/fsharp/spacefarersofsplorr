namespace Splorr.Spacefarers

open Elmish

module InGameUpdate =
    let private transformGame (transform:Game->Game) (model:Model) : Model * Cmd<Msg> =
        match model with
        | Model.InGame game ->
            game |> transform |> Model.InGame, Cmd.none
        | _ ->
            model, Cmd.none

    let updateChangeHeading (msg:ChangeOrderMsg<float<Degree> * float<Degree>>) (model : Model) : Model * Cmd<Msg> =
        match msg with
        | ChangeOrderMsg.Begin->
            model
            |> transformGame (fun game -> game |> Game.setState (GameState.SetHeading game.avatar.heading))
        | ChangeOrderMsg.Change x ->
            model
            |> transformGame (fun game -> game |> Game.setState (GameState.SetHeading x))
        | ChangeOrderMsg.Issue x ->
            model
            |> transformGame
                (fun game ->
                    game
                    |> Game.setHeading x)
        | ChangeOrderMsg.Belay ->
            model
            |> transformGame (fun game -> game |> Game.setState InSpace)

    let private updateMove = transformGame Game.moveAvatar

    let private headFor (id:string) = transformGame (Game.headFor id)

    let private dock (id:string) = transformGame (Game.dock id)

    let private probe (id:string) = transformGame (Game.probe id)

    let private undock = transformGame (Game.undock)

    let private viewStatus = transformGame Game.viewStatus

    let private viewVesselStatus = transformGame Game.viewVesselStatus

    let private viewEntityStatus (id:string) = transformGame (Game.viewEntityStatus id)

    let private goBack = transformGame Game.goBack

    let private abandonDelivery = transformGame Game.abandonDelivery

    let private wait = transformGame Game.wait

    let private acceptDelivery (msg:AcceptOrderMsg) =
        match msg with
        | Begin ->
            transformGame (Game.pendDelivery)
        | Issue ->
            transformGame (Game.acceptDelivery)
        | Belay ->
            transformGame (Game.unpendDelivery)


    let private updateSelectDestination (msg:ChangeOrderMsg<string option>) =
        match msg with
        | ChangeOrderMsg.Begin->
            transformGame (Game.setState (GameState.SetDestination None))

        | Change x ->
            transformGame (Game.setState (GameState.SetDestination x))

        | ChangeOrderMsg.Issue x ->
            match x with
            | Some id ->
                transformGame (Game.headFor id >> Game.setState InSpace)
            | _ ->
                transformGame (Game.setState InSpace)

        | ChangeOrderMsg.Belay ->
            transformGame (Game.setState InSpace)

    let private changeThrottle (id:string) (msg:ChangeOrderMsg<float>) =
        match msg with
        | ChangeOrderMsg.Begin->
            transformGame (Game.beginSetThrottle id)
        | Change x ->
            transformGame (Game.changeThrottle id x)
        | ChangeOrderMsg.Issue x ->
            transformGame (Game.issueSetThrottle id x)
        | ChangeOrderMsg.Belay ->
            transformGame (Game.belaySetThrottle)

    let private buyCommodity (descriptor:CommodityDescriptor) (marketCommodity:MarketCommodity) (commodity:Commodity) (order:ChangeOrderMsg<float<Serplathe>>) =
        match order with
        | ChangeOrderMsg.Begin ->
            transformGame (Game.beginCommodityBuy commodity)
        | Change x ->
            transformGame (Game.changeCommodityBuy commodity x)
        | ChangeOrderMsg.Issue x ->
            transformGame (Game.issueCommodityBuy descriptor marketCommodity commodity x)
        | ChangeOrderMsg.Belay ->
            transformGame (Game.belayCommodityBuy)

    let private sellCommodity (commodity:Commodity) (order:ChangeOrderMsg<float<Serplathe>>) =
        match order with
        | ChangeOrderMsg.Begin ->
            transformGame (Game.beginCommoditySell commodity)
        | ChangeOrderMsg.Change x ->
            transformGame (Game.changeCommoditySell commodity x)
        | ChangeOrderMsg.Issue x ->
            transformGame (Game.issueCommoditySell commodity x)
        | ChangeOrderMsg.Belay ->
            transformGame (Game.belayCommoditySell)

    let update (msg:InGameMsg) =
        match msg with
        | ChangeHeading x ->
            updateChangeHeading x

        | SelectDestination x ->
            updateSelectDestination x

        | Move ->
            updateMove

        | HeadFor s ->
            headFor s

        | Dock s ->
            dock s

        | Undock ->
            undock

        | ViewStatus ->
            viewStatus

        | ViewVesselStatus ->
            viewVesselStatus

        | GoBack ->
            goBack

        | AbandonDelivery ->
            abandonDelivery

        | AcceptDelivery x ->
            acceptDelivery x

        | Wait ->
            wait

        | ChangeThrottle (id, order) ->
            changeThrottle id order

        | Probe id ->
            probe id

        | ViewEntityStatus id ->
            viewEntityStatus id

        | BuyCommodity (descriptor, marketCommodity, commodity, order) ->
            buyCommodity descriptor marketCommodity commodity order

        | SellCommodity (commodity, order) ->
            sellCommodity commodity order

