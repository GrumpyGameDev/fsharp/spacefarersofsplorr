namespace Splorr.Spacefarers

open Elmish

module Update =
    let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
        match msg with
        | Msg.MainMenu x ->
            MainMenuUpdate.update x

        | ShowMainMenu ->
            Model.MainMenu, Cmd.none

        | Msg.StartGame x ->
            StartGameUpdate.update x currentModel

        | Msg.InGame x ->
            InGameUpdate.update x currentModel

